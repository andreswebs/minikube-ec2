# minikube-ec2

[![Launch Stack](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=windows-ansible&templateURL=https://s3.amazonaws.com/andreswebs.cfn.public/minikube-ec2-amzn-linux.yml)

## Getting started

This template deploys an AWS EC2 instance running Amazon Linux 2 with
[minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)
installed.

The `ec2-user` user can run `kubectl`, `docker` and `minikube` commands.

To start the cluster, run:

```sh
minikube start --vm-driver=none
```

To enable remote `kubectl` access, add `--listen-address=0.0.0.0` and
`--port=8443:8443` to the command.

## Kubernetes dashboard

To enable the k8s dashboard, run:

```
minikube addons enable metrics-server
minikube addons enable dashboard
```

To access the k8s dashboard remotely, run:

`minikube dashboard`

Take note of the random port number in the output:

`http://127.0.0.1:<random-port>/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/`

And then from your machine start an ssh tunnel:

`ssh -i </path/to/your/aws/ssh/key.pem> -L 30000:127.0.0.1:<random-port> ec2-user@<ec2-instance-public-ip>`

You will then be able to access the dashboard in your machine at:

`http://127.0.0.1:30000/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/`

(You can change the port 30000 value in the commands above to any available
local port.)

## AWS ECR

To use container images from AWS ECR, enable the registry-creds addon:

`minikube addons enable registry-creds`

## Remote access with kubectl

### 0. Inspect the minikube config (kubeconfig)

```sh
minikube kubectl -- config view | tee minikube.config.yaml
```

### 1. Copy minikube credentials from EC2

Copy the files `/home/ec2-user/.minikube/profiles/minikube/client.crt` and
`/home/ec2-user/.minikube/profiles/minikube/client.key` from the EC2 instance to
your local machine using `scp`.

For example:

```sh
key="</path/to/your/aws/ssh/key.pem>"
ip="<ec2-instance-public-ip>"
u="ec2-user"
```

```sh
scp -i "${key}" "${u}@${ip}":~/.minikube/profiles/minikube/client.crt ~/.kube/
```

```sh
scp -i "${key}" "${u}@${ip}":~/.minikube/profiles/minikube/client.key ~/.kube/
```

### 2. Set local kubectl config

Add the appropriate values to `~/.kube/config`. For example:

```sh
cat <<-EOT > ~/.kube/config
---
apiVersion: v1
kind: Config
clusters:
  - cluster:
      insecure-skip-tls-verify: true
      server: https://${ip}:8443
    name: minikube-ec2
contexts:
  - context:
      cluster: minikube-ec2
      user: minikube
    name: minikube-ec2
current-context: minikube-ec2
preferences: {}
users:
  - name: minikube
    user:
      client-certificate: ~/.kube/client.crt
      client-key: ~/.kube/client.key
EOT
```

## Author

- **Andre Silva**
